import unittest
import program
import random
from functools import reduce
import time
from math import log2


class FindParamsTest(unittest.TestCase):

    def test_find_params(self):

        # произвольное количество произвольных списков произвольной длины
        test_cases = [[random.uniform(-1000, 1000) for _ in range(random.randint(1, 1000))]
                      for _ in range(random.randint(1, 1000))]

        for case in test_cases:
            with self.subTest(case=case):
                expected = (case, min(case), max(case), sum(case),
                            reduce(lambda x, y: x * y, case))
                result = program.find_params(case)
                for i in range(5):
                    self.assertEqual(result[i], expected[i])


class ProgramSpeedTest(unittest.TestCase):

    def test_speed(self):  # хочу O(n)

        sizes = (5000, 25000, 125000, 625000, 3125000)
        n = 5  # пусть данные на каждом шаге увеличиваются в 5 раз
        for size in sizes:
            with open('test.txt', 'w', encoding='utf8') as f:
                f.write(' '.join([str(1) for _ in range(size)]))
                """
                Пусть данные будут одинакового формата, а не рандом, так как рандом может
                сгенерировать длинное число, которое будет читаться дольше, и тогда не получится
                отследить строгое увеличение данных в 5 раз
                """
            with self.subTest(case=size):
                start = time.time()
                program.find_params(program.read_data('test.txt'))
                test_time = time.time() - start
                if size != sizes[0]:
                    self.assertLess(abs(test_time / prev_time), n + ((n * log2(n) - n) / 2))
                    """
                    n + ((n * log2(n) - n) / 5), n = 5 взяла просто за погрешность 
                    (по такой логике, что после O(n) идет
                    сложность nlog(n) (насколько мы проходили), и наверное в качестве границы между 
                    этими двумя сложностями можно взять половину от промежутка между ними, 
                    так как опытным путем выяснила, что не обязательно 
                    от увеличения объема данных в n раз время будет увеличиваться РОВНО в n раз,
                    может быть небольшая погрешность, но отследить как именно она меняется с увеличением 
                    входа мне не дала память моего компьютера
                    """
            prev_time = test_time


class ExceptionTest(unittest.TestCase):

    def test_empty_file(self):
        empty_file = 'empty.txt'
        with open(empty_file, 'w'):
            pass

        with self.assertRaises(EOFError):
            program.find_params(program.read_data(empty_file))


if __name__ == '__main__':
    unittest.main()


