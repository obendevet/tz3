from typing import List
# Я СНАЧАЛА ИМПОРТИРОВАЛА МОДУЛЬ ДЕСАЙМАЛ, ЧТОБЫ ИЗБЕЖАТЬ ОШИБКИ ПЕРЕПОЛНЕНИЯ,
# НО В ИТОГЕ ОКАЗАЛОСЬ, ЧТО ОШИБКИ НЕ БУДЕТ В ТИПЕ FLOAT


def read_data(filename: str) -> List[float]:
    """
    Эта функция считывает из файла числа.
    """
    with open(filename, 'r', encoding='utf8') as f:
        data = f.read().split()
        if data:
            return list(map(float, data))
        else:
            raise EOFError


def find_params(nums: List[float]) -> tuple:
    """
    Эта функция ищет среди этих чисел минимальное число, максимальное число, считает их общую сумму и произведение.
    """
    min_num = nums[0]
    max_num = nums[0]
    sum_of_nums = 0.0
    mul_of_nums = 1.0  # чтобы не было переполнения
    for num in nums:
        if num < min_num:
            min_num = num
        if num > max_num:
            max_num = num
        sum_of_nums += num
        mul_of_nums *= num
    return nums, min_num, max_num, sum_of_nums, mul_of_nums


def printer(params: tuple) -> None:

    ALL_NUMS = 0
    MIN_NUM = 1
    MAX_NUM = 2
    SUM_OF_NUMS = 3
    MUL_OF_NUMS = 4

    print(f"В файле: {' '.join(map(str, params[ALL_NUMS]))}")
    print(f'Минимальное: {params[MIN_NUM]}')
    print(f'Максимальное: {params[MAX_NUM]}')
    print(f'Сумма: {params[SUM_OF_NUMS]}')
    print(f'Произведение: {params[MUL_OF_NUMS]}')


